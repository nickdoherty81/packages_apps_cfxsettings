package com.codefirex.settings.activities;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.util.Log;
import android.os.SystemProperties;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.provider.Settings;
import android.text.Spannable;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

import com.codefirex.settings.R;
import com.codefirex.settings.CFXPreferenceFragment;
import com.codefirex.settings.colorpick.ColorPreference;
import com.codefirex.settings.SettingsFragment;
import com.codefirex.settings.utils.CMDProcessor;
import com.codefirex.settings.utils.Helpers;
import com.codefirex.settings.widgets.SeekBarPreference;


import java.io.IOException;
import java.util.List;

public class ThemePrefs extends SettingsFragment implements OnPreferenceChangeListener {

    private final static String TAG = ThemePrefs.class.getSimpleName();
    private static final String UI_INVERTED_MODE = "darkui_settings";

    private static final int REQUEST_PICK_BOOT_ANIMATION = 203;

    private CheckBoxPreference mDarkUI;
    private ContentResolver mCr;
    private PreferenceScreen mPrefSet;

    private Preference mCustomBootAnimation;

    ColorPreference mNavBar;
    ColorPreference mStatusBar;
    Preference mStockColor;
    Context mContext;
    ContentResolver mResolver;
    SeekBarPreference mNavBarAlpha;
    SeekBarPreference mStatusBarAlpha;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.theme_settings);

        mContext = (Context) getActivity();
        mResolver = mContext.getContentResolver();
        mPrefSet = getPreferenceScreen();
        mCr = getContentResolver();

        /* Custom Boot Animation */
        mCustomBootAnimation = findPreference("custom_bootanimation");

        /* Navigation Bar Custom Colors */
        mNavBar = (ColorPreference) findPreference("interface_navbar_color");
        mNavBar.setProviderTarget(Settings.System.SYSTEMUI_NAVBAR_COLOR,
                Settings.System.SYSTEMUI_NAVBAR_COLOR_DEF);

        /* Status Bar Custom Colors */
        mStatusBar = (ColorPreference) findPreference("interface_statusbar_color");
        mStatusBar.setProviderTarget(Settings.System.SYSTEMUI_STATUSBAR_COLOR,
                Settings.System.SYSTEMUI_STATUSBAR_COLOR_DEF);

        /* Navigation Bar Custom Alpha */
        mNavBarAlpha = (SeekBarPreference) findPreference("navigation_bar_alpha");
        final float defaultNavAlpha = Settings.System.getFloat(getActivity()
                .getContentResolver(), Settings.System.NAVIGATION_BAR_ALPHA,
                1.0f);
        mNavBarAlpha.setInitValue(Math.round(defaultNavAlpha * 100));
        mNavBarAlpha.setOnPreferenceChangeListener(this);

        /* Status Bar Custom Alpha */
        mStatusBarAlpha = (SeekBarPreference) findPreference("status_bar_alpha");
        final float defaultStatusAlpha = Settings.System.getFloat(getActivity()
                .getContentResolver(), Settings.System.STATUS_BAR_ALPHA,
                1.0f);
        mStatusBarAlpha.setInitValue(Math.round(defaultStatusAlpha * 100));
        mStatusBarAlpha.setOnPreferenceChangeListener(this);

        /* DarkUI Toggle */
        mDarkUI = (CheckBoxPreference) findPreference("darkui_settings");
        mDarkUI.setChecked(Settings.Secure.getInt(getActivity().getContentResolver(),
                Settings.Secure.UI_INVERTED_MODE, 1) == 2);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mNavBarAlpha != null) {
            final float defaultNavAlpha = Settings.System.getFloat(getActivity()
                    .getContentResolver(), Settings.System.NAVIGATION_BAR_ALPHA,
                    1.0f);
            mNavBarAlpha.setInitValue(Math.round(defaultNavAlpha * 100));
        } else if(mStatusBarAlpha != null) {
            final float defaultStatusAlpha = Settings.System.getFloat(getActivity()
                    .getContentResolver(), Settings.System.STATUS_BAR_ALPHA,
                    1.0f);
            mStatusBarAlpha.setInitValue(Math.round(defaultStatusAlpha * 100));
	}
    }

    // list off apps which we restart just to be sure due that AOSP
    // does not every time reload all resources on onConfigurationChanged
    // or because some apps are just not programmed well on that part.
    private String mTRDSApps[] = new String[] {
        "com.android.contacts",
        "com.android.calendar",
        "com.android.email",
        "com.android.vending",
        "com.android.mms",
        "com.google.android.talk",
        "com.google.android.gm",
        "com.google.android.googlequicksearchbox",
        "com.google.android.youtube",
        "com.google.android.apps.genie.geniewidget",
        "com.google.android.apps.plus",
        "com.google.android.apps.maps"
    };

    public void SetDarkUI() {
        Helpers.restartSystemUI();
        ActivityManager am = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> pids = am.getRunningAppProcesses();
           for(int i = 0; i < pids.size(); i++) {
               ActivityManager.RunningAppProcessInfo info = pids.get(i);
               for (int j = 0; j < mTRDSApps.length; j++) {
                   if(info.processName.equalsIgnoreCase(mTRDSApps[j])) {
                        am.killBackgroundProcesses(mTRDSApps[j]);
                   }
               }
           }
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (preference == mNavBarAlpha) {
            float val = (float) (Integer.parseInt((String)newValue) * 0.01);
            return Settings.System.putFloat(getActivity().getContentResolver(),
                    Settings.System.NAVIGATION_BAR_ALPHA,
                    val);
        } else if (preference == mStatusBarAlpha) {
            float val = (float) (Integer.parseInt((String)newValue) * 0.01);
            return Settings.System.putFloat(getActivity().getContentResolver(),
                    Settings.System.STATUS_BAR_ALPHA,
                    val);
        }
        return false;
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        if (preference == mCustomBootAnimation) {
            PackageManager packageManager = getActivity().getPackageManager();
            Intent test = new Intent(Intent.ACTION_GET_CONTENT);
            test.setType("application/zip");
            List<ResolveInfo> list = packageManager.queryIntentActivities(test, PackageManager.GET_ACTIVITIES);
            if(list.size() > 0) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT, null);
                intent.setType("application/zip");
                startActivityForResult(intent, REQUEST_PICK_BOOT_ANIMATION);
            } else {
                //No app installed to handle the intent - file explorer required
                Toast.makeText(getActivity().getApplicationContext(), R.string.install_file_manager_error, Toast.LENGTH_SHORT).show();
            }
        } else if (preference == mDarkUI) {
            Settings.Secure.putInt(getActivity().getContentResolver(),
                    Settings.Secure.UI_INVERTED_MODE,
                    ((CheckBoxPreference) preference).isChecked() ? 2 : 1);
            SetDarkUI();
            return true;
        } else {
            // If we didn't handle it, let preferences handle it.
            return super.onPreferenceTreeClick(preferenceScreen, preference);
        }
        return true;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_PICK_BOOT_ANIMATION) {
                if (data==null) {
                    //Nothing returned by user, probably pressed back button in file manager
                    return;
                }

                String path = data.getData().getEncodedPath();

                Helpers.getMount("rw");
                //backup old boot animation
                new CMDProcessor().su.runWaitFor("mv /system/media/bootanimation.zip /system/media/bootanimation.backup");

                //Copy new bootanimation, give proper permissions
                new CMDProcessor().su.runWaitFor("cp "+ path +" /system/media/bootanimation.zip");
                new CMDProcessor().su.runWaitFor("chmod 644 /system/media/bootanimation.zip");

                Helpers.getMount("ro");
            }
        }
    }
}
