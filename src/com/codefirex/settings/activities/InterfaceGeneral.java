package com.codefirex.settings.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.preference.Preference.OnPreferenceChangeListener;
import android.provider.Settings;
import android.text.Spannable;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;

import com.codefirex.settings.R;
import com.codefirex.settings.CFXActivity;
import com.codefirex.settings.CFXPreferenceFragment;
import com.codefirex.settings.SettingsFragment;

public class InterfaceGeneral extends SettingsFragment
    implements OnPreferenceChangeListener {

    private final static String TAG = InterfaceGeneral.class.getSimpleName();

    private static final String PREF_CUSTOM_CARRIER_LABEL = "custom_carrier_label";
    private static final String PREF_DISABLE_FULLSCREEN_KEYBOARD = "disable_fullscreen_keyboard";
    private static final String SHOW_ENTER_KEY = "show_enter_key";
    private static final String VOLUME_KEY_CURSOR_CONTROL = "volume_key_cursor_control";
    private static final String VIBRATION_MULTIPLIER = "vibrator_multiplier";
    private ContentResolver mCr;
    private PreferenceScreen mPrefSet;
    private Context mContext;

    private ListPreference mVolumeKeyCursorControl;
    private ListPreference mVibrationMultiplier;
    private Preference mCustomLabel;
    private CheckBoxPreference mBatteryWarning;
    private CheckBoxPreference mShowEnterKey;
    String mCustomLabelText = null;
    CheckBoxPreference mDisableFullscreenKeyboard;

    ContentResolver mResolver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ContentResolver resolver = getActivity().getContentResolver();

        addPreferencesFromResource(R.xml.interface_general);

        mContext = getActivity();
        mPrefSet = getPreferenceScreen();
        mCr = getContentResolver();
        mResolver = mContext.getContentResolver();

        /* Custom Carrier Label */
        mCustomLabel = findPreference(PREF_CUSTOM_CARRIER_LABEL);
        updateCustomLabelTextSummary();

        /* Force Disable Fullscreen Keyboard */
        mDisableFullscreenKeyboard = (CheckBoxPreference) findPreference(PREF_DISABLE_FULLSCREEN_KEYBOARD);
        mDisableFullscreenKeyboard.setChecked(Settings.System.getInt(getActivity().getContentResolver(),
                Settings.System.DISABLE_FULLSCREEN_KEYBOARD, 0) == 1);

        /* Volume Key Cursor Control for Text Entry */
        mVolumeKeyCursorControl = (ListPreference) findPreference(VOLUME_KEY_CURSOR_CONTROL);
        if(mVolumeKeyCursorControl != null) {
            mVolumeKeyCursorControl.setOnPreferenceChangeListener(this);
            mVolumeKeyCursorControl.setValue(Integer.toString(Settings.System.getInt(getActivity()
                    .getContentResolver(), Settings.System.VOLUME_KEY_CURSOR_CONTROL, 0)));
            mVolumeKeyCursorControl.setSummary(mVolumeKeyCursorControl.getEntry());
        }

        /* Allow Disabling Low Battery Warnings */
        mBatteryWarning = (CheckBoxPreference) findPreference("eos_system_disable_battery_warning");
        if(mBatteryWarning != null) {
            mBatteryWarning.setChecked(Settings.System.getInt(mResolver,
                    Settings.System.SYSTEM_DISABLE_LOW_BATTERY_WARNING,
                    Settings.System.SYSTEM_DISABLE_LOW_BATTERY_WARNING_DEF)
                    == Settings.System.SYSTEM_DISABLE_LOW_BATTERY_WARNING_DEF ? false : true);
            mBatteryWarning.setOnPreferenceChangeListener(this);
        }

        /* Force Enter Key on Keyboard */
        mShowEnterKey = (CheckBoxPreference) findPreference(SHOW_ENTER_KEY);
        mShowEnterKey.setChecked(Settings.System.getInt(getActivity().getContentResolver(),
                Settings.System.FORMAL_TEXT_INPUT, 0) == 1);

        /* Globally Change the Vibration Multiplier */
        mVibrationMultiplier = (ListPreference) findPreference(VIBRATION_MULTIPLIER);
        if(mVibrationMultiplier != null) {
            mVibrationMultiplier.setOnPreferenceChangeListener(this);
	    String currentValue = Float.toString(Settings.Secure.getFloat(getActivity()
                    .getContentResolver(), Settings.Secure.VIBRATION_MULTIPLIER, 1));
            mVibrationMultiplier.setValue(currentValue);
            mVibrationMultiplier.setSummary(currentValue);
        }
    }

    private void updateCustomLabelTextSummary() {
        mCustomLabelText = Settings.System.getString(getActivity().getContentResolver(),
                Settings.System.CUSTOM_CARRIER_LABEL);
        if (mCustomLabelText == null || mCustomLabelText.length() == 0) {
            mCustomLabel.setSummary(R.string.custom_carrier_label_notset);
        } else {
            mCustomLabel.setSummary(mCustomLabelText);
        }
    }

    @Override
    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        if (preference == mCustomLabel) {
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

            alert.setTitle(R.string.custom_carrier_label_title);
            alert.setMessage(R.string.custom_carrier_label_explain);

            // Set an EditText view to get user input
            final EditText input = new EditText(getActivity());
            input.setText(mCustomLabelText != null ? mCustomLabelText : "");
            alert.setView(input);

            alert.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    String value = ((Spannable) input.getText()).toString();
                    Settings.System.putString(getActivity().getContentResolver(),
                            Settings.System.CUSTOM_CARRIER_LABEL, value);
                    updateCustomLabelTextSummary();
                    Intent i = new Intent();
                    i.setAction("com.android.settings.LABEL_CHANGED");
                    getActivity().getApplicationContext().sendBroadcast(i);
                }
            });
            alert.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    // Canceled.
                }
            });

            alert.show();
            return true;
        } else if (preference == mDisableFullscreenKeyboard) {
            boolean checked = ((CheckBoxPreference) preference).isChecked();
            Settings.System.putInt(getActivity().getContentResolver(),
                    Settings.System.DISABLE_FULLSCREEN_KEYBOARD, checked ? 1 : 0);
            return true;
        } else if (preference == mShowEnterKey) {
            Settings.System.putInt(getActivity().getContentResolver(),
                Settings.System.FORMAL_TEXT_INPUT, mShowEnterKey.isChecked() ? 1 : 0);
            return true;
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object value) {
        if (preference == mVolumeKeyCursorControl) {
            String volumeKeyCursorControl = (String) value;
            int val = Integer.parseInt(volumeKeyCursorControl);
            Settings.System.putInt(getActivity().getContentResolver(),
                    Settings.System.VOLUME_KEY_CURSOR_CONTROL, val);
            int index = mVolumeKeyCursorControl.findIndexOfValue(volumeKeyCursorControl);
            mVolumeKeyCursorControl.setSummary(mVolumeKeyCursorControl.getEntries()[index]);
            return true;
        } else if (preference == mVibrationMultiplier) {
            String currentValue = (String) value;
            float val = Float.parseFloat(currentValue);
            Settings.Secure.putFloat(getActivity().getContentResolver(),
                    Settings.Secure.VIBRATION_MULTIPLIER, val);
            mVibrationMultiplier.setSummary(currentValue);
            return true;
        } else if (preference == mBatteryWarning) {
            Settings.System.putInt(mResolver,
                    Settings.System.SYSTEM_DISABLE_LOW_BATTERY_WARNING,
                    ((Boolean) value).booleanValue() ? 1 : 0);
            return true;
        }
        return false;
    }
}
